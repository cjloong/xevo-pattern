# Overview
This is a collection of pattern for javascript. It is meant to create helper
functions for development of patterns.

## Memento
Memento will change the class to add undo/redo/save functions. All these functions acts on
class.data (ie. it remembers stuffs in the object.data structure)

###### Example

    class A {
        constructor() {
            this.data = {
                id: 0
            };
        }
        
        id(id=null) {
            if(id==null) return this.data.id;
            else {
                this.data.id = id;
                return this;
            }
        }
    }
    
    const MementoA = makeMemento(A);
    const a = new MementoA();
    
    a.id(1);
    a.id(100);
    a.undo();
    console.log(a.id()); // 1
    a.redo();
    console.log(a.id()); // 100
    
