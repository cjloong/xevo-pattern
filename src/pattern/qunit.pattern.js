var makeMemento = require("./Memento.js").makeMemento;

QUnit.module("Working Memory Memento", (hooks)=>{
	QUnit.module("_Development",(hooks)=>{
		hooks.beforeEach((assert) => {

		});
		hooks.afterEach((assert) => {

		});

		QUnit.test("Memento with simple function property", (assert)=>{
			var SimpleClass = class {
				constructor(){
					this.data = {
						id: 0
					}

				}
				id(newid=null) {
					if(newid==null) {
						return this.data.id;
					} else {
						this.data.id=newid;
						return this;
					}
				}
			}
			const MementoClass = makeMemento(SimpleClass);
			const memento = new MementoClass;

			memento.id(1);
			assert.ok(memento.id()===1, `${memento.id()}`);
			memento.save();

			memento.id(10);
			assert.ok(memento.id()===10, `${memento.id()}`);

			memento.undo();
			assert.ok(memento.id()===1, `${memento.id()}`);
			


		});

		QUnit.test("Memento with simple data property", (assert)=>{
			var SimpleClass = class {
				constructor() {
					this.data = {
						id: 0
					}
				}
			};

			const MementoClass = makeMemento(SimpleClass);
			const memento = new MementoClass;

			memento.data.id = 1;
			assert.ok(memento.data.id===1, `${memento.data.id}`)

			memento.save();
			memento.data.id = 10;
			assert.ok(memento.data.id===10, `${memento.data.id}`)

			memento.undo();
			assert.ok(memento.data.id===1, `${memento.data.id}`)
			memento.redo();
			assert.ok(memento.data.id===10, `${memento.data.id}`)

		});

		QUnit.test("Memento on a simple class property getter/setter", (assert)=>{
			var SimpleClass = class {
				constructor() {
					this.data = {
						id: 0
						, getIdCount:0
						, setIdCount:0
					}
				}
				
				get id() {
					this.data.getIdCount++;
					return this.data.id;
				}
				set id(id) {
					this.data.setIdCount++;
					this.data.id=id;
				}
				
			};
			
			const MementoClass = makeMemento(SimpleClass);
			const memento = new MementoClass();

			memento.id=1;
			assert.ok(memento.id===1, `${memento.id}`)
			
			memento.save();
			assert.ok(memento.id===1, `${memento.id}`)
			memento.id=100;
			assert.ok(memento.id===100, `${memento.id}`)
			memento.id=1000;
			assert.ok(memento.id===1000, `${memento.id}`)
			
			
			memento.undo()
			assert.ok(memento.id===1, `${memento.id}`)
//			memento.undo()
//			assert.ok(memento.id===1, `${memento.id}`)
//
//			assert.ok(false, "TODO");
		});
	});
});

