module.exports = {};

module.exports.makeMemento = function (clazz) {
	let mementoClass = class extends clazz {
		constructor() {
			super();
			this.memento = new clazz(...arguments);
			let self = this;

			Object.getOwnPropertyNames(this).filter(n=>n!="memento").forEach( (name)=>{
				console.debug(`relinking ${name}`);
				this[name] = this.memento[name];
			});

 			// Linking all prototype functions
 			Object.getOwnPropertyNames(clazz.prototype).filter(n=>n!="constructor").forEach((name)=>{
				console.debug(`----------Processing:${name}`);
 				self[name] = function() {
 					return self.memento[name](...arguments);
 				}
 			});

			// Make memento member vars
			this._undo = [];
			this._redo = [];

			// Push first state to undo stack
			this._undo.push(JSON.stringify(this.memento.data));

		}

		save() {
			this._undo.push(JSON.stringify(this.memento.data));
		}

		undo() {
			if(this._undo.length<1) throw "Cannot undo further.";

			var currentJson = JSON.stringify(this.memento.data);
			var json = this._undo.pop();

			console.debug(json);
			
			
			if(currentJson==json) {
				if(this._undo.length===0) {
					// nothing changed and we are at the beginning
					this._undo.push(json);
					throw "Nothing to undo.";
				}
				// we just saved and no changes after save
				json = this._undo.pop();
			}

			this._redo.push(currentJson);
			this.memento.data = JSON.parse(json);
			this.data = this.memento.data;
		}

		redo() {
			if(this._redo.length<1) throw "Cannot redo further.";

			var currentJson = JSON.stringify(this.memento.data);
			var json = this._redo.pop();
			this._undo.push(currentJson);

			this.memento.data = JSON.parse(json);
			this.data = this.memento.data;

		}
	}

	return mementoClass;
}



// WEBPACK FOOTER //
// ./C:/Users/chekjen/git/evo3-pattern/src/pattern/Memento.js