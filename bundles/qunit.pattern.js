/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var makeMemento = __webpack_require__(1).makeMemento;

QUnit.module("Working Memory Memento", function (hooks) {
	QUnit.module("_Development", function (hooks) {
		hooks.beforeEach(function (assert) {});
		hooks.afterEach(function (assert) {});

		QUnit.test("Memento on a simple class", function (assert) {
			var MementoClass = makeMemento(function () {
				function _class() {
					_classCallCheck(this, _class);

					this.id = 0;
				}

				_createClass(_class, [{
					key: "id",
					get: function get() {
						return this.id;
					},
					set: function set(id) {
						this.id = id;
					}
				}]);

				return _class;
			}());
			var memento = new MementoClass();

			memento.id = 1;

			assert.ok(false, "TODO");
		});
	});
});

/***/ },
/* 1 */
/***/ function(module, exports) {

"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

module.exports = {};
//TODO: Refactor this to a separate library
module.exports.makeMemento = function (clazz) {
	var mementoClass = function (_clazz) {
		_inherits(mementoClass, _clazz);

		function mementoClass() {
			_classCallCheck(this, mementoClass);

			var _this = _possibleConstructorReturn(this, (mementoClass.__proto__ || Object.getPrototypeOf(mementoClass)).call(this));

			_this.memento = new (Function.prototype.bind.apply(clazz, [null].concat(Array.prototype.slice.call(arguments))))();
			var self = _this;

			Object.getOwnPropertyNames(_this).filter(function (n) {
				return n != "memento";
			}).forEach(function (name) {
				console.debug("relinking " + name);
				_this[name] = _this.memento[name];
			});

			// Linking all prototype functions
			Object.getOwnPropertyNames(clazz.prototype).filter(function (n) {
				return n != "constructor";
			}).forEach(function (name) {
				self[name] = function () {
					var _self$memento;

					return (_self$memento = self.memento)[name].apply(_self$memento, arguments);
				};
			});

			// Make memento member vars
			_this._undo = [];
			_this._redo = [];

			// Push first state to undo stack
			_this._undo.push(JSON.stringify(_this.memento.data));

			return _this;
		}

		_createClass(mementoClass, [{
			key: "save",
			value: function save() {
				this._undo.push(JSON.stringify(this.memento.data));
			}
		}, {
			key: "undo",
			value: function undo() {
				if (this._undo.length < 1) throw "Cannot undo further.";

				var currentJson = JSON.stringify(this.memento.data);
				var json = this._undo.pop();

				if (currentJson == json) {
					if (this._undo.length === 0) {
						// nothing changed and we are at the beginning
						this._undo.push(json);
						throw "Nothing to undo.";
					}
					// we just saved and no changes after save
					json = this._undo.pop();
				}

				this._redo.push(currentJson);
				this.memento.data = JSON.parse(json);
			}
		}, {
			key: "redo",
			value: function redo() {
				if (this._redo.length < 1) throw "Cannot redo further.";

				var currentJson = JSON.stringify(this.memento.data);
				var json = this._redo.pop();
				this._undo.push(currentJson);

				this.memento.data = JSON.parse(json);
			}
		}]);

		return mementoClass;
	}(clazz);

	return mementoClass;
};

/***/ }
/******/ ]);
//# sourceMappingURL=qunit.pattern.js.map